package com.cesi.cda.courspoo;

import com.cesi.cda.courspoo.dao.persone.Person;
import org.junit.jupiter.api.Test;


import static org.junit.jupiter.api.Assertions.assertEquals;


import com.cesi.cda.courspoo.controllers.PersonController;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class PersonTest {

    @Test
    void testCreatePerson() {
        Person person = new Person(1, "David", "Lansonneur", 1995, "France");
        assertEquals(1, person.getId());
        assertEquals("David", person.getNom());
        assertEquals("Lansonneur", person.getPrenom());
        assertEquals(1995, person.getAnneeNaissance());
        assertEquals("France", person.getNationalite());

        // Verifying if the person is created successfully
        assertEquals("Person posted", PersonController.postPerson(person));
    }
}
