package com.cesi.cda.courspoo.services;

import com.cesi.cda.courspoo.dao.persone.Person;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class PersonService {

    // Method to get a person by ID
    public static Person getPersonById(int id) {
        Person person = null;
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {
            connection = DriverManager.getConnection("jdbc:h2:mem:dev-base", "romain", "romain");

            statement = connection.prepareStatement("SELECT * FROM PERSON WHERE id = ?");
            statement.setInt(1, id);

            resultSet = statement.executeQuery();

            if (resultSet.next()) {
                person = new Person(
                        resultSet.getInt("id"),
                        resultSet.getString("nom"),
                        resultSet.getString("prenom"),
                        resultSet.getInt("anneeNaissance"),
                        resultSet.getString("nationalite")
                );
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (resultSet != null) resultSet.close();
                if (statement != null) statement.close();
                if (connection != null) connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return person;
    }

    // Method to add a new person
    public static void addPerson(Person person) {
        Connection connection = null;
        PreparedStatement statement = null;

        try {
            connection = DriverManager.getConnection("jdbc:h2:mem:dev-base", "romain", "romain");

            statement = connection.prepareStatement("INSERT INTO PERSON (nom, prenom, anneeNaissance, nationalite) VALUES (?, ?, ?, ?)");
            statement.setString(1, person.getNom());
            statement.setString(2, person.getPrenom());
            statement.setInt(3, person.getAnneeNaissance());
            statement.setString(4, person.getNationalite());

            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (statement != null) statement.close();
                if (connection != null) connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    // Method to delete a person by ID
    public static void deletePersonById(int id) {
        Connection connection = null;
        PreparedStatement statement = null;

        try {
            connection = DriverManager.getConnection("jdbc:h2:mem:dev-base", "romain", "romain");

            statement = connection.prepareStatement("DELETE FROM PERSON WHERE id = ?");
            statement.setInt(1, id);

            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (statement != null) statement.close();
                if (connection != null) connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    // Method to get all persons
    public static List<Person> getAllPersons() {
        List<Person> persons = new ArrayList<>();
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;

        try {
            connection = DriverManager.getConnection("jdbc:h2:mem:dev-base", "romain", "romain");

            statement = connection.createStatement();

            resultSet = statement.executeQuery("SELECT * FROM PERSON");

            while (resultSet.next()) {
                Person person = new Person(
                        resultSet.getInt("id"),
                        resultSet.getString("nom"),
                        resultSet.getString("prenom"),
                        resultSet.getInt("anneeNaissance"),
                        resultSet.getString("nationalite")
                );
                persons.add(person);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (resultSet != null) resultSet.close();
                if (statement != null) statement.close();
                if (connection != null) connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return persons;
    }

    // Method to update a person
    public static void updatePerson(Person person) {
        Connection connection = null;
        PreparedStatement statement = null;

        try {
            connection = DriverManager.getConnection("jdbc:h2:mem:dev-base", "romain", "romain");

            statement = connection.prepareStatement("UPDATE PERSON SET nom = ?, prenom = ?, anneeNaissance = ?, nationalite = ? WHERE id = ?");
            statement.setString(1, person.getNom());
            statement.setString(2, person.getPrenom());
            statement.setInt(3, person.getAnneeNaissance());
            statement.setString(4, person.getNationalite());
            statement.setInt(5, person.getId());

            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (statement != null) statement.close();
                if (connection != null) connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

}
