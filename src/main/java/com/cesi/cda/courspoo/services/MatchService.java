package com.cesi.cda.courspoo.services;

import com.cesi.cda.courspoo.dao.Match;
import com.cesi.cda.courspoo.dao.meet.Rencontre;
import com.cesi.cda.courspoo.dao.persone.Person;

import java.util.List;

public class MatchService {

    public static Match createMatch(Rencontre rencontre, Person winner, Person loser) {
        String nomGagnant = winner.getNom();
        String nomPerdant = loser.getNom();
        String dateLieuRencontre = rencontre.getLieuTournoi() + " - " + rencontre.getAnnee();
        int sumAge = calculateSumAge(winner, loser);
        return new Match(nomGagnant, nomPerdant, dateLieuRencontre, sumAge);
    }


    private static int calculateSumAge(Person person1, Person person2) {
        int age1 = java.time.Year.now().getValue() - person1.getAnneeNaissance();
        int age2 = java.time.Year.now().getValue() - person2.getAnneeNaissance();
        return age1 + age2;
    }
}
