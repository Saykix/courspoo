package com.cesi.cda.courspoo.services;

import com.cesi.cda.courspoo.dao.meet.Rencontre;

import java.sql.*;

public class RencontreService {

    // Method to get a rencontre by ID
    public static Rencontre getRencontreById(int id) {
        Rencontre rencontre = null;
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {
            connection = DriverManager.getConnection("jdbc:h2:mem:dev-base", "romain", "romain");

            statement = connection.prepareStatement("SELECT * FROM RENCONTRE WHERE id = ?");
            statement.setInt(1, id);

            resultSet = statement.executeQuery();

            if (resultSet.next()) {
                rencontre = new Rencontre(
                        resultSet.getInt("id"),
                        resultSet.getInt("NuGAGNANT"),
                        resultSet.getInt("NuPERDANT"),
                        resultSet.getString("LIEUTOURNOI"),
                        resultSet.getInt("ANNEE")
                );
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (resultSet != null) resultSet.close();
                if (statement != null) statement.close();
                if (connection != null) connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return rencontre;
    }

}
