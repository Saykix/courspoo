package com.cesi.cda.courspoo.controllers;

import com.cesi.cda.courspoo.dao.Match;
import com.cesi.cda.courspoo.dao.meet.Rencontre;
import com.cesi.cda.courspoo.dao.persone.Person;
import com.cesi.cda.courspoo.dao.meet.Rencontre;
import com.cesi.cda.courspoo.services.MatchService;
import com.cesi.cda.courspoo.controllers.PersonController;
import com.cesi.cda.courspoo.controllers.RencontreController;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/match")
public class MatchController {

    @PostMapping("/create")
    public Match createMatch(@RequestParam("renconteId") int renconteId, @RequestParam("winnerId") int winnerId, @RequestParam("loserId") int loserId) {

        Person winner = PersonController.getPerson(winnerId);
        Person loser = PersonController.getPerson(loserId);
        Rencontre rencontre = RencontreController.getRencontre(renconteId);

        return MatchService.createMatch(rencontre, winner, loser);
    }
}
