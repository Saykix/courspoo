package com.cesi.cda.courspoo.controllers;

import com.cesi.cda.courspoo.dao.persone.Person;
import com.cesi.cda.courspoo.services.PersonService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/person")
public class PersonController {

    @GetMapping("/{id}")
    public static Person getPerson(@PathVariable int id) {

        return PersonService.getPersonById(id);
    }

    @PostMapping()
    public static String postPerson(@RequestBody Person person) {

        PersonService.addPerson(person);
        return "Person posted";
    }

    @PutMapping("/{id}")
    public String updatePerson(@PathVariable int id, @RequestBody Person person) {

        person.setId(id);
        PersonService.updatePerson(person);
        return "Person updated";
    }

    @DeleteMapping("/{id}")
    public String deletePerson(@PathVariable int id) {

        PersonService.deletePersonById(id);
        return "Person deleted";
    }

    @GetMapping("/all")
    public List<Person> getAllPersons() {

        return PersonService.getAllPersons();
    }
}
