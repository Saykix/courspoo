package com.cesi.cda.courspoo.controllers;


import com.cesi.cda.courspoo.dao.meet.Rencontre;
import com.cesi.cda.courspoo.services.RencontreService;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/rencontre")
public class RencontreController {

    @GetMapping("/{id}")
    public static Rencontre getRencontre(@PathVariable int id) {

        return RencontreService.getRencontreById(id);
    }

    @PostMapping()
    public String postRencontre() {

        return "Rencontre posted";
    }

    @PutMapping("/{id}")
    public String updateRencontre(@PathVariable int id) {

        return "Rencontre updated";
    }

    @DeleteMapping("/{id}")
    public String deleteRencontre(@PathVariable int id) {

        return "Rencontre deleted";
    }
}
