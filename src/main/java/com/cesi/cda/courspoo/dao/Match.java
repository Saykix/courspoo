package com.cesi.cda.courspoo.dao;

public class Match {
    String nomGagnant;
    String nomPerdant;
    String dateLieuRencontre;
    int sumAge;

    public Match(String nomGagnant, String nomPerdant, String dateLieuRencontre, int sumAge) {
        this.nomGagnant = nomGagnant;
        this.nomPerdant = nomPerdant;
        this.dateLieuRencontre = dateLieuRencontre;
        this.sumAge = sumAge;
    }

    public String getNomGagnant() {
        return nomGagnant;
    }

    public void setNomGagnant(String nomGagnant) {
        this.nomGagnant = nomGagnant;
    }

    public String getNomPerdant() {
        return nomPerdant;
    }

    public void setNomPerdant(String nomPerdant) {
        this.nomPerdant = nomPerdant;
    }

    public String getDateLieuRencontre() {
        return dateLieuRencontre;
    }

    public void setDateLieuRencontre(String dateLieuRencontre) {
        this.dateLieuRencontre = dateLieuRencontre;
    }

    public int getsumAge() {
        return sumAge;
    }

    public void setsumAge(int sumAge) {
        this.sumAge = sumAge;
    }
}
