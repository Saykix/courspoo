package com.cesi.cda.courspoo.dao.meet;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Rencontre {
    private int id;
    private int nuGagnant;
    private int nuPerdant;
    private String lieuTournoi;
    private int annee;

    public Rencontre(int id, int nuGagnant, int nuPerdant, String lieuTournoi, int annee) {

        this.id = id;
        this.nuGagnant = nuGagnant;
        this.nuPerdant = nuPerdant;
        this.lieuTournoi = lieuTournoi;
        this.annee = annee;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNuGagnant() {
        return nuGagnant;
    }

    public void setNuGagnant(int nuGagnant) {
        this.nuGagnant = nuGagnant;
    }

    public int getNuPerdant() {
        return nuPerdant;
    }

    public void setNuPerdant(int nuPerdant) {
        this.nuPerdant = nuPerdant;
    }

    public String getLieuTournoi() {
        return lieuTournoi;
    }

    public void setLieuTournoi(String lieuTournoi) {
        this.lieuTournoi = lieuTournoi;
    }

    public int getAnnee() {
        return annee;
    }

    public void setAnnee(int annee) {
        this.annee = annee;
    }
}
